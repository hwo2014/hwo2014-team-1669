﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using HelloWorldOpen.Constants;

namespace HelloWorldOpen
{
    public class MsgWrapper
    {
        private string
            _gameId;

        [JsonProperty("msgType")]
        public string MsgType { get; set; }

        [JsonProperty("data")]
        public Object Data { get; set; }

        [JsonProperty("gameId")]
        public string GameId
        {
            get { return _gameId ?? string.Empty; }
            set { _gameId = value; }
        }

        public bool ShouldSerializeGameId()
        {
            return MsgType == MessageTypes.CarPositions || MsgType == MessageTypes.Crash ||
                   MsgType == MessageTypes.Spawn || MsgType == MessageTypes.LapFinished ||
                   MsgType == MessageTypes.Disqualified;
        }

        [JsonProperty("gameTick")]
        public int GameTick { get; set; }

        public bool ShouldSerializeGameTick()
        {
            // Todo add game tick to throttle message to keep in sync...

            return MsgType == MessageTypes.CarPositions || MsgType == MessageTypes.Crash ||
                   MsgType == MessageTypes.Spawn || MsgType == MessageTypes.LapFinished ||
                   MsgType == MessageTypes.Disqualified;
        }

        public MsgWrapper(string msgType, Object data)
        {
            MsgType = msgType;

            switch (MsgType)
            {
                case Constants.MessageTypes.YourCar:
                    Data = JsonConvert.DeserializeObject<CarData>(data.ToString());
                    break;
                case Constants.MessageTypes.Spawn:
                    Data = JsonConvert.DeserializeObject<CarData>(data.ToString());
                    break;
                case Constants.MessageTypes.GameInit:
                    Data = JsonConvert.DeserializeObject<InitData>(data.ToString());
                    break;
                case Constants.MessageTypes.CarPositions:
                    Data = JsonConvert.DeserializeObject<IEnumerable<CarPositionData>>(data.ToString());
                    break;
                default:
                    Data = data;
                    break;
            }
        }
    }

    public sealed class InitData
    {
        [JsonProperty("race")]
        public RaceData Race { get; set; }

        #region Nested classes

        public sealed class RaceData
        {
            [JsonProperty("track")]
            public TrackData Track { get; set; }

            [JsonProperty("cars")]
            public IEnumerable<CarData> Cars { get; set; }

            [JsonProperty("raceSession")]
            public RaceSessionData RaceSession { get; set; }

            #region Nested classes

            public sealed class TrackData
            {
                [JsonProperty("id")]
                public string Id { get; set; }

                [JsonProperty("name")]
                public string Name { get; set; }

                [JsonProperty("pieces")]
                public List<TrackPieceData> Pieces { get; set; }

                [JsonProperty("lanes")]
                public IEnumerable<TrackLaneData> Lanes { get; set; }

                [JsonProperty("startingPoint")]
                public StartingPointData StartingPoint { get; set; }

                #region Nested classes

                public sealed class TrackPieceData
                {
                    [JsonProperty("length")]
                    public double Length { get; set; }

                    [JsonProperty("switch")]
                    public bool Switch { get; set; }

                    [JsonProperty("radius")]
                    public double Radius { get; set; }

                    [JsonProperty("angle")]
                    public double Angle { get; set; }
                }
                public sealed class TrackLaneData
                {
                    [JsonProperty("distanceFromCenter")]
                    public double DistanceFromCenter { get; set; }

                    [JsonProperty("index")]
                    public int Index { get; set; }
                }
                public sealed class StartingPointData
                {
                    [JsonProperty("position")]
                    public PositionData Position { get; set; }

                    [JsonProperty("angle")]
                    public double Angle { get; set; }

                    #region Nested classes

                    public sealed class PositionData
                    {
                        [JsonProperty("x")]
                        public double X { get; set; }

                        [JsonProperty("y")]
                        public double Y { get; set; }
                    }

                    #endregion
                }

                #endregion
            }

            public class CarData
            {
                [JsonProperty("id")]
                public CarIdData Id { get; set; }

                [JsonProperty("dimensions")]
                public DimensionsData Dimensions { get; set; }

                #region Nested classes

                public class CarIdData
                {
                    [JsonProperty("name")]
                    public string Name { get; internal set; }

                    [JsonProperty("color")]
                    public string Color { get; internal set; }
                }
                public class DimensionsData
                {
                    [JsonProperty("length")]
                    public double Length { get; set; }

                    [JsonProperty("width")]
                    public double Width { get; set; }

                    [JsonProperty("guideFlagPosition")]
                    public double GuideFlagPosition { get; set; }
                }

                #endregion
            }

            public class RaceSessionData
            {
                [JsonProperty("laps")]
                public int Laps { get; set; }

                [JsonProperty("maxLapTimeMs")]
                public int MaxLapTimeMs { get; set; }

                [JsonProperty("quickRace")]
                public bool QuickRace { get; set; }
            }

            #endregion
        }

        #endregion
    }

    public sealed class CarData
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("color")]
        public string Color { get; set; }
    }

    public sealed class CarPositionData
    {
        [JsonProperty("id")]
        public IdData Id { get; set; }

        [JsonProperty("angle")]
        public double Angle { get; set; }

        [JsonProperty("piecePosition")]
        public PiecePositionData PiecePosition { get; set; }

        #region Nested classes

        public class IdData
        {
            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("color")]
            public string Color { get; set; }
        }

        public class PiecePositionData
        {
            [JsonProperty("pieceIndex")]
            public int PieceIndex { get; set; }

            [JsonProperty("inPieceDistance")]
            public double InPieceDistance { get; set; }

            [JsonIgnore]
            public double LapDistance { get; set; }

            [JsonIgnore]
            public double CurrentPerTickSpeed { get; set; }

            [JsonIgnore]
            public double CurrentThrottle { get; set; }

            [JsonIgnore]
            public bool CurrentlyCrashed { get; set; }

            [JsonProperty("lane")]
            public LaneData Lane { get; set; }

            [JsonProperty("lap")]
            public int Lap { get; set; }

            #region Nested classes

            public class LaneData
            {
                [JsonProperty("startLaneIndex")]
                public int StartLaneIndex { get; set; }

                [JsonProperty("endLaneIndex")]
                public int EndLaneIndex { get; set; }
            }

            #endregion
        }

        #endregion
    }
}
