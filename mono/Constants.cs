﻿namespace HelloWorldOpen
{
    namespace Constants
    {
        public static class MessageTypes
        {
            public const string CarPositions = "carPositions";
            public const string Join         = "join";
            public const string GameInit     = "gameInit";
            public const string GameEnd      = "gameEnd";
            public const string GameStart    = "gameStart";
            public const string YourCar      = "yourCar";
            public const string Disqualified = "dnf";
            public const string Throttle     = "throttle";
            public const string Crash        = "crash";
            public const string Spawn        = "spawn";
            public const string LapFinished  = "lapFinished";
            public const string Ping         = "ping";
            public const string Switch       = "switchLane";
        }

        public static class SwitchDirections
        {
            public const string Left = "Left";
            public const string Right = "Right";
        }
    }
}
