﻿using System;
using System.Globalization;
using System.IO;

namespace Utilities
{
    public enum LogType
    {
        Information,
        Warning,
        Error
    }

    public class LogEventArgs
    {
        public LogType Type { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }

        public LogEventArgs(LogType type, string message)
        {
            Type = type;
            Message = message;
            Exception = null;
        }

        public LogEventArgs(LogType type, string message, Exception exception)
        {
            Type = type;
            Message = message;
            Exception = exception;
        }
    }

    public class Log
    {
        #region SINGLETON

        private static Log _instance;
        private string _collectedLogs;

        public string Filepath { get; set; }
        public bool CollectLogs { get; set; }

        public string CollectedLogs
        {
            get { return _collectedLogs; }
        }

        private Log()
        {
            Filepath = string.Empty;
            CollectLogs = false;
            _collectedLogs = string.Empty;
        }

        public static Log Instance
        {
            get { return _instance ?? (_instance = new Log()); }
        }

        #endregion

        #region EVENTS

        public delegate void LogEventHandler(object sender, LogEventArgs e);

        public event LogEventHandler LogEvent;

        #endregion

        #region ACTUAL LOG

        public void LogIt(LogType type, string message)
        {
            if (!string.IsNullOrEmpty(Filepath))
                WriteToFile(type, message, null, Filepath);
            if (LogEvent != null)
                LogEvent(this, new LogEventArgs(type, message));
            if (CollectLogs)
                Collect(type, message, null);
        }

        public void LogIt(LogType type, string message, Exception exception)
        {
            if (!string.IsNullOrEmpty(Filepath))
                WriteToFile(type, message, exception, Filepath);
            if (LogEvent != null)
                LogEvent(this, new LogEventArgs(type, message, exception));
            if (CollectLogs)
                Collect(type, message, exception);
        }

        private void WriteToFile(LogType type, string message, Exception exception, string path)
        {
            try
            {
                using (var w = File.AppendText(path))
                {
                    var now = DateTime.Now;
                    w.WriteLine(now.ToString(CultureInfo.InvariantCulture) + "\t" + type.ToString() + "\t" + message);
                    if (exception == null) return;
                    w.WriteLine("\t\t\t" + exception.Message);
                    w.WriteLine("\t\t\t" + exception.StackTrace);
                }
            }
            catch (Exception ex)
            {
                LogEvent(this, new LogEventArgs(LogType.Error, ex.Message, ex));
            }
        }

        private void Collect(LogType type, string message, Exception exception)
        {
            var now = DateTime.Now;

            _collectedLogs +=
                now.ToString(CultureInfo.InvariantCulture) + "\t" + type.ToString() + "\t" + message;
            if (exception != null)
                _collectedLogs +=
                    Environment.NewLine +
                    "\t\t\t" + exception.Message + Environment.NewLine +
                    "\t\t\t" + exception.StackTrace;
            _collectedLogs +=
                Environment.NewLine;
        }

        #endregion
    }
}