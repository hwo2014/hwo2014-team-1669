using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using HelloWorldOpen;
using Newtonsoft.Json;
using HelloWorldOpen.Constants;
using Utilities;

// ReSharper disable once CheckNamespace
public class Bot
{
    private readonly StreamWriter _writer;

    /// <summary>
    /// The initial data as received from the HWO servers. \n
    /// Includes track information, other cars, etc...
    /// </summary>
    private InitData _initData;

    /// <summary>
    /// A backlog of all received car position updates.
    /// </summary>
    private readonly List<IEnumerable<CarPositionData>> _carPositionBacklog;
    
    /// <summary>
    /// The currently assigned id data of the bot.
    /// </summary>
    private readonly CarData _myCar;
    
    /// <summary>
    /// True, if the bot is currently off track because it crashed.
    /// </summary>
    private bool _crashed;

    public static void Main(string[] args)
    {
        var host    = args[0];
        var port    = int.Parse(args[1]);
        var botName = args[2];
        var botKey  = args[3];

        Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        Log.Instance.Filepath = string.Format("log_{0}.txt", DateTime.Now.ToString("yyyyMMddHHmmss"));
        Log.Instance.LogEvent += (sender, eventArgs) => Console.WriteLine(eventArgs.Message);

        Log.Instance.LogIt(LogType.Information, "--- Start");

        using(var client = new TcpClient(host, port))
        {
            var stream = client.GetStream();
            var reader = new StreamReader(stream);
            var writer = new StreamWriter(stream) {AutoFlush = true};

            new Bot(reader, writer, new Join(botName, botKey));
        }

        Log.Instance.LogIt(LogType.Information, "--- End");
    }

    Bot(StreamReader reader, StreamWriter writer, Join join)
    {
        _carPositionBacklog = new List<IEnumerable<CarPositionData>>();
        _writer = writer;
        string line;

        Send(join);

        while((line = reader.ReadLine()) != null)
        {
            var msg = JsonConvert.DeserializeObject<MsgWrapper>(line);

            switch(msg.MsgType)
            {
                case MessageTypes.CarPositions:
                    AddPositionDataToBacklog((IEnumerable<CarPositionData>)msg.Data);
                    HandleCarPositionsUpdate(msg);
                    Log.Instance.LogIt(LogType.Information, "CarPositions: " + line);
                    break;
                case MessageTypes.Join:
                    Log.Instance.LogIt(LogType.Information, "Joined");
                    Send(new Ping());
                    break;
                case MessageTypes.YourCar:
                    Log.Instance.LogIt(LogType.Information, "Got my car: " + line);
                    _myCar = (CarData) msg.Data;
                    Send(new Ping());
                    break;
                case MessageTypes.GameInit:
                    HandleInitialization(msg);
                    Send(new Ping());
                    Log.Instance.LogIt(LogType.Information, "GameInit: " + line);
                    break;
                case MessageTypes.GameEnd:
                    Log.Instance.LogIt(LogType.Information, "Race ended");
                    Send(new Ping());
                    LogRaceData();
                    break;
                case MessageTypes.GameStart:
                    Log.Instance.LogIt(LogType.Information, "Race starts");
                    Send(new Ping());
                    break;
                case MessageTypes.Disqualified:
                    Log.Instance.LogIt(LogType.Information, "Disqualified");
                    Send(new Ping());
                    break;
                case MessageTypes.Crash:
                    Log.Instance.LogIt(LogType.Warning, "Crash: " + line);
                    _crashed = true;
                    Send(new Ping());
                    break;
                case MessageTypes.Spawn:
                    if (((CarData) msg.Data).Name == _myCar.Name && ((CarData) msg.Data).Color == _myCar.Color)
                        _crashed = false;
                    Send(new Ping());
                    break;
                default:
                    Log.Instance.LogIt(LogType.Information, "Unhandled: " + msg.MsgType + "; " + line);
                    Send(new Ping());
                    break;
            }
        }
    }

    /// <summary>
    /// Send a message to the HWO servers.
    /// </summary>
    /// <param name="msg">The <see cref="SendMsg"/>.</param>
    private void Send(SendMsg msg)
    {
        var json = msg.ToJson();

        // Todo: Remove bc debug
        Log.Instance.LogIt(LogType.Information, string.Format("Sending {0}", json));

        _writer.WriteLine(json);
    }

    #region Utilities

    /// <summary>
    /// Add position updates to the backlog (<see cref="_carPositionBacklog"/>) and calculate per tick speed and lap distance.
    /// </summary>
    /// <param name="data">The car position update data from HWO servers.</param>
    private void AddPositionDataToBacklog(IEnumerable<CarPositionData> data)
    {
        if (!_carPositionBacklog.Any())
        {
            _carPositionBacklog.Add(data);
            return;
        }

        // calculate per tick speed
        var positionData = data.FirstOrDefault(d => d.Id.Color == _myCar.Color && d.Id.Name == _myCar.Name);

        if (positionData == null)
            return;

        var lastPositionData =
            _carPositionBacklog.Last()
                .FirstOrDefault(x => x.Id.Name == positionData.Id.Name && x.Id.Color == positionData.Id.Color);

        if (lastPositionData == null)
        {
            Log.Instance.LogIt(LogType.Error, "AddPositionDataToBacklog: lastPositionData == null");
        }
        else if (lastPositionData.PiecePosition.PieceIndex == positionData.PiecePosition.PieceIndex)
        {
            // Last and current position updates are on the same track piece.
            // We just need to add up the in piece distances.

            positionData.PiecePosition.LapDistance = positionData.PiecePosition.InPieceDistance +
                                                        (lastPositionData.PiecePosition.LapDistance -
                                                        lastPositionData.PiecePosition.InPieceDistance);
            positionData.PiecePosition.CurrentPerTickSpeed = positionData.PiecePosition.LapDistance -
                                                                lastPositionData.PiecePosition.LapDistance;

            if (positionData.PiecePosition.CurrentPerTickSpeed < 0)
            {
                Log.Instance.LogIt(LogType.Warning,
                    "Result speed is negative: " + positionData.PiecePosition.CurrentPerTickSpeed);
                positionData.PiecePosition.CurrentPerTickSpeed = lastPositionData.PiecePosition.CurrentPerTickSpeed;
            }
        }
        if (_initData.Race.Track.Pieces.ElementAt(lastPositionData.PiecePosition.PieceIndex).Angle.CompareTo(0) != 0)
        {
            // The previous track piece was a bent one.
            // We need to calculate the length of the bent piece, the distance traveled in this piece
            // and add the current in piece distance.

            var lastPiece =
                _initData.Race.Track.Pieces.First(
                    x => _initData.Race.Track.Pieces.IndexOf(x) == lastPositionData.PiecePosition.PieceIndex);

            var lastUsedStartLaneIndex = lastPositionData.PiecePosition.Lane.StartLaneIndex;
            var lastUsedEndLaneIndex = lastPositionData.PiecePosition.Lane.EndLaneIndex;
            var approxUsedLaneLength = CalculateBentLaneLength(lastPiece, lastUsedStartLaneIndex,
                lastUsedEndLaneIndex);

            var traveledDistance = positionData.PiecePosition.InPieceDistance +
                                    (approxUsedLaneLength -
                                    lastPositionData.PiecePosition.InPieceDistance);

            positionData.PiecePosition.LapDistance = lastPositionData.PiecePosition.LapDistance + traveledDistance;

            positionData.PiecePosition.CurrentPerTickSpeed = positionData.PiecePosition.LapDistance -
                                                                lastPositionData.PiecePosition.LapDistance;

            if (positionData.PiecePosition.CurrentPerTickSpeed < 0)
            {
                Log.Instance.LogIt(LogType.Warning,
                    "Result speed is negative: " + positionData.PiecePosition.CurrentPerTickSpeed);
                positionData.PiecePosition.CurrentPerTickSpeed = lastPositionData.PiecePosition.CurrentPerTickSpeed;
            }
        }
        else
        {
            // The previous track piece was a straight one.
            // We need to calculate the distance traveled in the last piece and add
            // the current in piece distance.

            var lastPiece =
                _initData.Race.Track.Pieces.First(
                    x => _initData.Race.Track.Pieces.IndexOf(x) == lastPositionData.PiecePosition.PieceIndex);
                
            double traveledDistance;

            if (lastPositionData.PiecePosition.Lane.StartLaneIndex ==
                lastPositionData.PiecePosition.Lane.EndLaneIndex)
            {
                traveledDistance = positionData.PiecePosition.InPieceDistance +
                                    (lastPiece.Length -
                                    lastPositionData.PiecePosition.InPieceDistance);
            }
            else
                traveledDistance = positionData.PiecePosition.InPieceDistance +
                                    (CalculateSwitchPieceLength(lastPiece,
                                        lastPositionData.PiecePosition.Lane.StartLaneIndex,
                                        lastPositionData.PiecePosition.Lane.EndLaneIndex) -
                                    lastPositionData.PiecePosition.InPieceDistance);

            positionData.PiecePosition.LapDistance = lastPositionData.PiecePosition.LapDistance + traveledDistance;

            positionData.PiecePosition.CurrentPerTickSpeed = positionData.PiecePosition.LapDistance -
                                                                lastPositionData.PiecePosition.LapDistance;

            if (positionData.PiecePosition.CurrentPerTickSpeed < 0)
            {
                Log.Instance.LogIt(LogType.Warning,
                    "Result speed is negative: " + positionData.PiecePosition.CurrentPerTickSpeed);
                positionData.PiecePosition.CurrentPerTickSpeed = lastPositionData.PiecePosition.CurrentPerTickSpeed;
            }
        }

        _carPositionBacklog.Add(data);
    }

    /// <summary>
    /// Calculate the length of a bent track piece and take lane offsets into consideration. \n
    /// Start lane and end lane may be different if the car has switched lanes in this piece. \n
    /// </summary>
    /// <param name="trackPiece">The bent track piece.</param>
    /// <param name="startLaneIndex">The index of the start lane.</param>
    /// <param name="endLaneIndex">The index of the end lane.</param>
    /// <returns>The length of the bent lane.</returns>
    private double CalculateBentLaneLength(InitData.RaceData.TrackData.TrackPieceData trackPiece, int startLaneIndex, int endLaneIndex)
    {
        var lastUsedStartLane = _initData.Race.Track.Lanes.ElementAt(startLaneIndex);
        var factor = trackPiece.Angle >= 0 ? -1 : 1;
        var startLanePieceLength = (2d * (trackPiece.Radius + factor * lastUsedStartLane.DistanceFromCenter) * Math.PI *
                                    Math.Abs(trackPiece.Angle)) / 360d;
        var lastUsedEndLane = _initData.Race.Track.Lanes.ElementAt(endLaneIndex);
        var endLanePieceLength = (2d * (trackPiece.Radius + factor * lastUsedEndLane.DistanceFromCenter) * Math.PI *
                                  Math.Abs(trackPiece.Angle)) / 360d;
        var approxUsedLaneLength = (startLanePieceLength / 2d) + (endLanePieceLength / 2d);

        return approxUsedLaneLength;
    }

    private double CalculateSwitchPieceLength(InitData.RaceData.TrackData.TrackPieceData trackPiece, int startLaneIndex,
        int endLaneIndex)
    {
        var x1 = 0d;
        var x2 = trackPiece.Length;
        var y1 = _initData.Race.Track.Lanes.ElementAt(startLaneIndex).DistanceFromCenter;
        var y2 = _initData.Race.Track.Lanes.ElementAt(endLaneIndex).DistanceFromCenter;
        return Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
    }

    private void LogRaceData()
    {
        using (var writer = new StreamWriter("racedata.csv"))
        {
            foreach (var positionData in _carPositionBacklog)
            {
                var data = positionData.FirstOrDefault(x => x.Id.Color == _myCar.Color && x.Id.Name == _myCar.Name);
                if(data == null)
                    continue;

                var switching = data.PiecePosition.Lane.StartLaneIndex == data.PiecePosition.Lane.EndLaneIndex ? 0 : 1;

                var trackPiece = _initData.Race.Track.Pieces.ElementAt(data.PiecePosition.PieceIndex);
                var trackPieceLength = trackPiece.Angle > 0
                    ? CalculateBentLaneLength(trackPiece, data.PiecePosition.Lane.StartLaneIndex,
                        data.PiecePosition.Lane.EndLaneIndex)
                    : (switching == 1
                        ? CalculateSwitchPieceLength(trackPiece, data.PiecePosition.Lane.StartLaneIndex,
                            data.PiecePosition.Lane.EndLaneIndex)
                        : trackPiece.Length);

                //Tick; Crashed; Throttle; Geschwindigkeit; Winkel (Auto); Winkel Lane; Radius Lane; L�nge Lane; Switching?;
                writer.WriteLine("{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8}",
                    _carPositionBacklog.IndexOf(positionData),
                    data.PiecePosition.CurrentlyCrashed ? 1 : 0,
                    data.PiecePosition.CurrentThrottle, 
                    data.PiecePosition.CurrentPerTickSpeed,
                    data.Angle, 
                    trackPiece.Angle, 
                    trackPiece.Radius,
                    trackPieceLength,
                    switching);
            }
        }
    }

    #endregion

    #region AI

    /// <summary>
    /// Handle initializations for the AI.
    /// </summary>
    /// <param name="msg">The initialization message from HWO servers.</param>
    private void HandleInitialization(MsgWrapper msg)
    {
        // Todo: AI initialization

        Log.Instance.LogIt(LogType.Information, "Race init");
        _initData = (InitData) msg.Data;
    }

    /// <summary>
    /// Handle car position updates.
    /// </summary>
    /// <param name="msg">The car position update message from HWO servers.</param>
    private void HandleCarPositionsUpdate(MsgWrapper msg)
    {
        if (_myCar == null)
        {
            Log.Instance.LogIt(LogType.Error, "HandleCarPositionsUpdate: _myCar == null");
            return;
        }

        var data =
            ((IEnumerable<CarPositionData>) msg.Data).FirstOrDefault(
                x => x.Id.Name == _myCar.Name && x.Id.Color == _myCar.Color);

        if (data == null)
        {
            Log.Instance.LogIt(LogType.Error, "HandleCarPositionsUpdate: data == null");
            return;
        }

        data.PiecePosition.CurrentlyCrashed = _crashed;

        if (_crashed)
        {
            Log.Instance.LogIt(LogType.Warning, "Crashed");
            Send(new Ping());
            return;
        }

        var trackPieces = _initData.Race.Track.Pieces.ToList();
        
        var switchPieceIndexes = from p in trackPieces where p.Switch select trackPieces.IndexOf(p);

        if (switchPieceIndexes.Any(x => x == data.PiecePosition.PieceIndex + 1) &&
            data.PiecePosition.Lane.StartLaneIndex == data.PiecePosition.Lane.EndLaneIndex)
        {
            Send(data.PiecePosition.Lane.StartLaneIndex == 0
                ? new Switch(SwitchDirections.Right)
                : new Switch(SwitchDirections.Left));
            return;
        }
        var throttle = 0.65;
        data.PiecePosition.CurrentThrottle = throttle;
        Send(new Throttle(throttle));

        Log.Instance.LogIt(LogType.Information, "Current speed: " + data.PiecePosition.CurrentPerTickSpeed);
        Log.Instance.LogIt(LogType.Information, "Current throttle: " + throttle);
    }

    #endregion
}

