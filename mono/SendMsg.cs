﻿using System;
using HelloWorldOpen.Constants;
using Newtonsoft.Json;

namespace HelloWorldOpen
{
    public abstract class SendMsg
    {
        public string ToJson()
        {
            var result = JsonConvert.SerializeObject(new MsgWrapper(MsgType(), MsgData()));
            return result;
        }
        protected virtual Object MsgData()
        {
            return this;
        }

        protected abstract string MsgType();
    }

    public class Join : SendMsg
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("color")]
        public string Color { get; set; }

        public Join(string name, string key)
        {
            Name = name;
            Key = key;
            Color = "red";
        }

        protected override string MsgType()
        {
            return MessageTypes.Join;
        }
    }

    public class Ping : SendMsg
    {
        protected override string MsgType()
        {
            return MessageTypes.Ping;
        }
    }

    public class Throttle : SendMsg
    {
        [JsonProperty("value")]
        public double Value { get; set; }

        public Throttle(double value)
        {
            Value = value;
        }

        protected override Object MsgData()
        {
            return Value;
        }

        protected override string MsgType()
        {
            return MessageTypes.Throttle;
        }
    }

    public class Switch : SendMsg
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        public Switch(string value)
        {
            Value = value;
        }

        protected override Object MsgData()
        {
            return Value;
        }

        protected override string MsgType()
        {
            return MessageTypes.Switch;
        }
    }
}
